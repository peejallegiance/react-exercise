// client/src/App.js

import React from "react";
import "./App.css";
import { useState } from "react";
import Table from "./components/Table";


function App() {

  const [reps, setReps] = useState([]);
  const [state, setState] = useState(0);
  const [type, setType] = useState(0);

  const searchReps = async (repType, repState) => {
    const response = await fetch("http://localhost:3001/" + repType + "/" + repState);
    const data = await response.json();
    console.log(repState);
    setReps(data.results);
    console.log(data.results);
  }

  const setStateSelection = (e) => {
    setState(e.target.value)
    //searchReps('senators', e.target.value);
  };

  const setTypeSelection = (e) => {
    setType(e.target.value)
    //searchReps(e.target.value, 'oh');
  };

  const onClick = () => {
    if(type === "0" || state === "0"){
      //display error
      alert("Please make your selections and try again.");
    }
    else if (type === 0|| state === 0){
      //display error
      alert("Please make your selections and try again.");
    }
    else{
      searchReps(type, state);
    }
  };

  return (
    <div className="App">
      <div className="container mt-3">
      <div className="row">
      <h3 style={ {color: "#03a7ed", float: "left" }}><strong>Who's My Representative?</strong></h3>
      <hr />
      </div>  
      <div className="row">
        <div className="col-4">
          <select required id="stateSelection" name="stateSelection" onChange={setStateSelection}>
            <option value="0">Select a State</option>
            <option value="AL">Alabama</option>
            <option value="AK">Alaska</option>
            <option value="AZ">Arizona</option>
            <option value="AR">Arkansas</option>
            <option value="CA">California</option>
            <option value="CO">Colorado</option>
            <option value="CT">Connecticut</option>
            <option value="DE">Delaware</option>
            <option value="DC">District Of Columbia</option>
            <option value="FL">Florida</option>
            <option value="GA">Georgia</option>
            <option value="HI">Hawaii</option>
            <option value="ID">Idaho</option>
            <option value="IL">Illinois</option>
            <option value="IN">Indiana</option>
            <option value="IA">Iowa</option>
            <option value="KS">Kansas</option>
            <option value="KY">Kentucky</option>
            <option value="LA">Louisiana</option>
            <option value="ME">Maine</option>
            <option value="MD">Maryland</option>
            <option value="MA">Massachusetts</option>
            <option value="MI">Michigan</option>
            <option value="MN">Minnesota</option>
            <option value="MS">Mississippi</option>
            <option value="MO">Missouri</option>
            <option value="MT">Montana</option>
            <option value="NE">Nebraska</option>
            <option value="NV">Nevada</option>
            <option value="NH">New Hampshire</option>
            <option value="NJ">New Jersey</option>
            <option value="NM">New Mexico</option>
            <option value="NY">New York</option>
            <option value="NC">North Carolina</option>
            <option value="ND">North Dakota</option>
            <option value="OH">Ohio</option>
            <option value="OK">Oklahoma</option>
            <option value="OR">Oregon</option>
            <option value="PA">Pennsylvania</option>
            <option value="RI">Rhode Island</option>
            <option value="SC">South Carolina</option>
            <option value="SD">South Dakota</option>
            <option value="TN">Tennessee</option>
            <option value="TX">Texas</option>
            <option value="UT">Utah</option>
            <option value="VT">Vermont</option>
            <option value="VA">Virginia</option>
            <option value="WA">Washington</option>
            <option value="WV">West Virginia</option>
            <option value="WI">Wisconsin</option>
            <option value="WY">Wyoming</option>
          </select>
        </div>
        <div className="col-4">
          <select required id="typeSelection" name="typeSelection" onChange={setTypeSelection}>
            <option value="0">Select a Type</option>
            <option value="representatives">Representatives</option>
            <option value="senators">Senators</option>
          </select>
        </div>
        <div className="col-4"><button type="button" className="btn btn-primary" onClick={onClick}>Find</button></div>
      </div>
      <div className="row">
        <div className="col-6">
          <h3 style = {{ float: "left" }}>List {type !== 0 ? '/' : ''} <span style={ {color: "#03a7ed" }}>{type !== 0 ? type.charAt(0).toUpperCase() + type.slice(1) : '' }</span></h3>
        </div>
        <div className="col-6">
          <h3>Info</h3>
        </div>
      </div>
      <div className="row">
          <Table reps={reps} />
      </div>
      </div>
    </div>
  );
}


export default App;