import { useState } from 'react';

const Table = ( {reps} ) => {
  const [rep, setRep]= useState([]);


  const handleClick = (index, rep, e) => {
    console.log(e.target.innerHTML + index + rep);
    setRep(rep);
  }

  return (
    <>
    <div className="col-6">
      {reps.length > 0 ? 
      <table className="table">
          <thead>
              <tr className="table-secondary">
                  <th scope="col">Name</th>
                  <th scope="col">Party</th>
              </tr>
          </thead>
          <tbody>
            { 
              reps.map((rep, index) => (
                <tr key={rep.name} onClick={(e) => handleClick(index, rep, e)}>
                <td>{rep.name}</td>
                <td>{rep.party.charAt(0)}</td>
                </tr>
              ))
            }
          </tbody>
      </table>
      : <span style={{float: "left"}}>No results to show.  Make a selection above.</span>}
    </div>
    <div className="col-6">
      <fieldset disabled>
        <div className="form-group">
          {rep.name ? 
          <><input type="text" id="disabledTextInput" className="form-control mb-2" placeholder={rep.name ? rep.name.split(" ")[0] : "Make a Selection"} />
          <input type="text" id="disabledTextInput" className="form-control mb-2" placeholder={rep.name ? rep.name.split(" ")[1] : "Make a Selection"} />
          <input type="text" id="disabledTextInput" className="form-control mb-2" placeholder={rep.district > 0 ? rep.district : "No District to show"} />
          <input type="text" id="disabledTextInput" className="form-control mb-2" placeholder={rep.phone} />
          <textarea type="text" id="disabledTextInput" className="form-control mb-2" placeholder={rep.office} />
          <div className="input details mb-2"><a href={rep.link}>{rep.link}</a></div></>
          : "Select a Person on the list to see more details."}
        </div>
      </fieldset>
    </div>
  </>
)}

export default Table